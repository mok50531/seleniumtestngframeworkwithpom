package unitedWebsitePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class UnitedWebsiteHomePage {

    public UnitedWebsiteHomePage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".app-components-GlobalHeader-globalHeader__globalBottomNav--2aHvD>a")
    public List<WebElement> mainMenuItems;

    @FindBy(css = ".app-components-BookTravel-bookTravel__travelNavContainer--2c4IY>div>ul>li")
    public List<WebElement> bookTravelMenuNavigationItems;

    @FindBy(css = "label[for='roundtrip']")
    public WebElement roundTripRadioButton;

    @FindBy(css = "label[for='oneway']")
    public WebElement oneWayTripRadioButton;

    @FindBy(xpath = "//input[@id='award']/parent::div/label")
    public WebElement bookWithMilesCheckbox;

    @FindBy(id = "flexDatesLabel")
    public WebElement FlexibleDatesCheckbox;

    @FindBy(id = "bookFlightOriginInput")
    public WebElement bookFlightFrom;

    @FindBy(css = "button[class='atm-c-btn app-components-QueryBuilder-styles__clearButton--dzha6 app-components-QueryBuilder-styles__panelIsOpen--2oyfn atm-c-btn--bare']")
    public WebElement clearTheFlightFromForm;

    @FindBy(id = "bookFlightDestinationInput")
    public WebElement bookFlightTo;

    @FindBy(id = "DepartDate")
    public WebElement departDate;

    @FindBy(css = ".app-containers-BookCalendar-bookCalendar__prevIconContainer--1CBAY")
    public WebElement arrow;

    @FindBy(xpath = "((//div[@class='CalendarMonth CalendarMonth_1'])[2]//tbody/tr)[6]/td")
    public WebElement jan30Element;

    @FindBy(css = ".app-components-PassengerSelector-passengers__passengerButton--w8CX7")
    public WebElement travelersNumberSelectionBox;

    @FindBy(id = "cabinType")
    public WebElement flightClass;

    @FindBy(id = "cabinType_item-2")
    public WebElement firstOrBusiness;

    @FindBy(xpath = "//span[text()='Find flights']/parent::button")
    public WebElement findFlightsButton;

    @FindBy(tagName = "h2")
    public WebElement successMessage;
}
