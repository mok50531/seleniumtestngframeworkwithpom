package utilities;

import org.openqa.selenium.WebElement;

public class CheckboxValidate {

    public static void validateCheckBoxDisplayed(WebElement element){
        element.isDisplayed();
    }

    public static void validateCheckBoxEnabled(WebElement element){
        element.isEnabled();
    }

    public static void validateCheckBoxSelected(WebElement element){
        element.isSelected();
    }

    public static boolean validateCheckBoxAll(WebElement element){
        return element.isDisplayed() && element.isSelected() && element.isEnabled();
    }

    public static boolean validateCheckBoxNotSelected(WebElement element){
        return !element.isSelected();
    }
}
