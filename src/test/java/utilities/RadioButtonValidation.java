package utilities;

import org.openqa.selenium.WebElement;

public class RadioButtonValidation {

    public static void validateDisplayed(WebElement element){
        element.isDisplayed();
    }

    public static void validateSelected(WebElement element){
        element.isSelected();
    }

    public static void validateEnabled(WebElement element){
        element.isEnabled();
    }

    public static void validateAll(WebElement element){
        if (element.isEnabled() && element.isSelected()) {
            element.isDisplayed();
        }
    }

    public static void validateNotSelected(WebElement element){
        element.isSelected();
    }
}
