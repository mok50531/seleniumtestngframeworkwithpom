package scripts;

import org.testng.Assert;
import org.testng.annotations.Test;
import texts.ExpectedTextsForUnited;
import utilities.CheckboxValidate;
import utilities.RadioButtonValidation;
import utilities.Waiter;

import java.nio.charset.StandardCharsets;

public class UnitedAirlineTest extends Base{

    @Test(testName = "Validate Main menu", priority = 1)
    public void validateMainMenu(){
        /*
        Given user is on "https://www.united.com/en/us"
        Then user should see “Main menu” navigation items
        |BOOK                              |
        |MY TRIPS                          |
        |TRAVEL INFO                       |
        |MILEAGEPLUS® PROGRAM              |
        |DEALS                             |
        |HELP                              |
         */
        for (int i = 0; i < ExpectedTextsForUnited.expectedMainMenuItems.length; i++) {
            Assert.assertTrue(unitedWebsiteHomePage.mainMenuItems.get(i).isDisplayed());
            Assert.assertEquals(unitedWebsiteHomePage.mainMenuItems.get(i).getText(), ExpectedTextsForUnited.expectedMainMenuItems[i]);
        }

    }

    @Test(testName = "Validate Book Travel menu navigation items", priority = 2)
    public void validateBookTravelMenuNavigationItems(){
        /*
        Given user is on "https://www.united.com/en/us"
        Then user should see "Book travel menu" navigation items
        |Book            |
        |Flight Status   |
        |Check-in        |
        |My trips        |
         */
        for (int i = 0; i < unitedWebsiteHomePage.bookTravelMenuNavigationItems.size(); i++) {
            Assert.assertTrue(unitedWebsiteHomePage.bookTravelMenuNavigationItems.get(i).isDisplayed());
            Assert.assertEquals(unitedWebsiteHomePage.bookTravelMenuNavigationItems.get(i).getText(), ExpectedTextsForUnited.expectedBookTravelMenuItems[i]);
        }
    }

    @Test(testName = "Validate Round-trip and One-way radio buttons", priority = 3)
    public void validateRoundTripAndOneWayRadioButtons(){
        /*
        Given user is on "https://www.united.com/en/us"
        Then validate "Roundtrip" radio button is displayed, is enabled and is selected
        And validate "One-way" radio button is displayed, is enabled but is not selected
        When user clicks on "One-way" radio button
        Then validate "One-way" radio button is selected while "Roundtrip" radio button is deselected
         */

        RadioButtonValidation.validateAll(unitedWebsiteHomePage.roundTripRadioButton);
        RadioButtonValidation.validateEnabled(unitedWebsiteHomePage.oneWayTripRadioButton);
        RadioButtonValidation.validateDisplayed(unitedWebsiteHomePage.oneWayTripRadioButton);
        RadioButtonValidation.validateNotSelected(unitedWebsiteHomePage.oneWayTripRadioButton);

        unitedWebsiteHomePage.oneWayTripRadioButton.click();

        RadioButtonValidation.validateNotSelected(unitedWebsiteHomePage.roundTripRadioButton);
        RadioButtonValidation.validateAll(unitedWebsiteHomePage.oneWayTripRadioButton);
    }

    @Test(testName = "Validate Book with miles and Flexible dates checkboxes", priority = 4)
    public void validateBookWithMilesAndFlexibleDatesCheckboxes(){
        /*
        Given user is on "https://www.united.com/en/us"
        Then validate "Book with miles" checkbox is displayed, is enabled but is not selected
        And validate "Flexible dates" checkbox is displayed, is enabled but is not selected
        When user clicks both checkboxes
        Then validate both checkboxes are selected
        When user clicks on both selected checkboxes again
        Then validate both checkboxes are deselected
         */

        CheckboxValidate.validateCheckBoxDisplayed(unitedWebsiteHomePage.bookWithMilesCheckbox);
        CheckboxValidate.validateCheckBoxNotSelected(unitedWebsiteHomePage.bookWithMilesCheckbox);
        CheckboxValidate.validateCheckBoxEnabled(unitedWebsiteHomePage.bookWithMilesCheckbox);

        CheckboxValidate.validateCheckBoxDisplayed(unitedWebsiteHomePage.FlexibleDatesCheckbox);
        CheckboxValidate.validateCheckBoxNotSelected(unitedWebsiteHomePage.FlexibleDatesCheckbox);
        CheckboxValidate.validateCheckBoxEnabled(unitedWebsiteHomePage.FlexibleDatesCheckbox);

        unitedWebsiteHomePage.FlexibleDatesCheckbox.click();
        unitedWebsiteHomePage.bookWithMilesCheckbox.click();

        CheckboxValidate.validateCheckBoxAll(unitedWebsiteHomePage.FlexibleDatesCheckbox);
        CheckboxValidate.validateCheckBoxAll(unitedWebsiteHomePage.bookWithMilesCheckbox);

        unitedWebsiteHomePage.FlexibleDatesCheckbox.click();
        unitedWebsiteHomePage.bookWithMilesCheckbox.click();

        CheckboxValidate.validateCheckBoxDisplayed(unitedWebsiteHomePage.bookWithMilesCheckbox);
        CheckboxValidate.validateCheckBoxNotSelected(unitedWebsiteHomePage.bookWithMilesCheckbox);
        CheckboxValidate.validateCheckBoxEnabled(unitedWebsiteHomePage.bookWithMilesCheckbox);

        CheckboxValidate.validateCheckBoxDisplayed(unitedWebsiteHomePage.FlexibleDatesCheckbox);
        CheckboxValidate.validateCheckBoxNotSelected(unitedWebsiteHomePage.FlexibleDatesCheckbox);
        CheckboxValidate.validateCheckBoxEnabled(unitedWebsiteHomePage.FlexibleDatesCheckbox);
    }

    @Test(testName = "Validate one-way ticket search results from Chicago, IL, US (ORD) to Miami, FL, US (MIA)", priority = 5)
    public void validateOneWayTicketFromChicagoToMiami(){
        /*
        Given user is on "https://www.united.com/en/us"
        When user selects "One-way" ticket radio button
        And user enters "Chicago, IL, US (ORD)" to from input box
        And user enters "Miami, FL, US (MIA)" to input box
        And user selects "Jan 30" to the dates input box
        And user selects "2 Adults" from travelers selector
        And user selects "Business or First" from cabin dropdown
        And user clicks on "Find Flights" button
        Then validate departure equals to "Depart: Chicago, IL, US to Miami, FL, US
         */

        unitedWebsiteHomePage.oneWayTripRadioButton.click();
        unitedWebsiteHomePage.bookFlightFrom.click();
        unitedWebsiteHomePage.clearTheFlightFromForm.click();
        unitedWebsiteHomePage.bookFlightFrom.sendKeys(ExpectedTextsForUnited.chicagoInfo);
        unitedWebsiteHomePage.bookFlightTo.click();
        unitedWebsiteHomePage.bookFlightTo.sendKeys(ExpectedTextsForUnited.miamiInfo);
        unitedWebsiteHomePage.departDate.click();
        unitedWebsiteHomePage.arrow.click();
        unitedWebsiteHomePage.arrow.click();
        unitedWebsiteHomePage.jan30Element.click();
        unitedWebsiteHomePage.travelersNumberSelectionBox.sendKeys("2");
        unitedWebsiteHomePage.flightClass.click();
        unitedWebsiteHomePage.firstOrBusiness.click();
        unitedWebsiteHomePage.findFlightsButton.click();
        Assert.assertTrue(unitedWebsiteHomePage.successMessage.isDisplayed());
        Assert.assertEquals(unitedWebsiteHomePage.successMessage.getText(), ExpectedTextsForUnited.departSuccessMessageValidation);
    }
}
