package scripts;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import unitedWebsitePages.UnitedWebsiteHomePage;
import utilities.ConfigReader;
import utilities.Driver;

public class Base {

    WebDriver driver;
    UnitedWebsiteHomePage unitedWebsiteHomePage;

    @BeforeMethod
    public void setUp(){
        driver = Driver.getDriver();
        unitedWebsiteHomePage = new UnitedWebsiteHomePage(driver);
        driver.get(ConfigReader.getProperty("unitedWebURL"));
    }

    @AfterMethod
    public void teardown(){
        Driver.teardown();
    }
}
