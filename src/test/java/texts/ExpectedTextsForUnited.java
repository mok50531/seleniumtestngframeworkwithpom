package texts;

public class ExpectedTextsForUnited {

    public static String[] expectedMainMenuItems = {"BOOK", "MY TRIPS", "TRAVEL INFO", "MILEAGEPLUS® PROGRAM", "DEALS", "HELP"};
    public static String[] expectedBookTravelMenuItems = {"Book", "Flight status", "Check-in", "My trips"};

    public static final String departSuccessMessageValidation = "Depart: Chicago, IL, US to Miami, FL, US";
    public static final String chicagoInfo = "Chicago, IL, US (ORD)";
    public static final String miamiInfo = "Miami, FL, US (MIA)";
}
